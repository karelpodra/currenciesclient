
let currencies =[];
let currencyId = 0;

// Initialization after the HTML document has been loaded...
window.addEventListener('load', async () => {

    // Function calls for initial page loading activities...
    doLoadCurrencies();
});

async function doLoadCurrencies() {
    currencies = await fetchCurrencies();
    renderCurrencies();
}

function renderCurrencies() {

    let currenciesDiv = document.querySelector('#currenciesList');
    let currenciesHtml = '<hr>';
    for (let currency of currencies) {
        currenciesHtml = currenciesHtml + /*html*/`
            <div>
                ${currency.name} ${currency.rate} 
                <button onclick="displayCurrencyEditPopup(${currency.id})">Update</button>
                <button onclick="displayCurrencyDeletePopup(${currency.id})">Delete currency</button>
                <br>
            </div>
        `;
    }
    currenciesDiv.innerHTML = currenciesHtml;
}



/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/


//EDIT
// Step 1: popup-akna avamine
async function displayCurrencyEditPopup(currentId) {
    await openPopup(POPUP_CONF_DEFAULT, 'currencyEditTemplate');

    let currency = currencies.find(c => c.id === currentId);
    currencyId = currency.id;


    let currencyNameInputElement = document.querySelector('#currencyEditName');
    let currencyRateInputElement = document.querySelector('#currencyEditRate');

    currencyNameInputElement.value = currency.name;
    currencyRateInputElement.value = currency.rate;

}

//Step 2: andmete salvestamine (REST-rakendusele saatmine)
async function editCurrency() {
    let currencyNameInputElement = document.querySelector('#currencyEditName');
    let currencyRateInputElement = document.querySelector('#currencyEditRate');

    let currency = {
        id: currencyId,
        name: currencyNameInputElement.value,
        rate: currencyRateInputElement.value,
    };

    await updateCurrency(currency);
    await doLoadCurrencies()
    closePopup();
}


//ADD
// CurrencyAdd: Step 1: popup akna avamine
async function displayCurrencyAddingPopup() {
    await openPopup(POPUP_CONF_DEFAULT, 'currencyAddTemplate');
}

// CurrencyAdd: Step 2: andmete salvestamine (REST-rakendusele saatmine)
async function addNewCurrency() {
    let currencyNameInputElement = document.querySelector('#currencyAddName');
    let currencyRateInputElement = document.querySelector('#currencyAddRate');

    let currency = {
        name: currencyNameInputElement.value,
        rate: currencyRateInputElement.value,
    };

    await postCurrency(currency);
    await doLoadCurrencies();
    closePopup();
}


//DELETE
// CurrencyAdd: Step 1: popup akna avamine
async function displayCurrencyDeletePopup(deleteId) {
    await openPopup(POPUP_CONF_DEFAULT, 'currencyDeleteTemplate');
    let currency = currencies.find(c => c.id === deleteId);
    currencyId = currency.id;
}

// CurrencyAdd: Step 2: andmete salvestamine (REST-rakendusele saatmine)
async function doDeleteCurrency() {
    let currency = currencies.find(c => c.id === currencyId);
    
    await deleteCurrency(currency);
    await doLoadCurrencies();
    closePopup();
}
