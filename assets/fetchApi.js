
async function fetchCurrencies() {
    let result = await fetch(`${API_URL}/currencies/all`);
    let currencies = await result.json();
    return currencies;
}

async function postCurrency(currency) {
    await fetch(`${API_URL}/currencies/add`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(currency)
    });
    
}

async function updateCurrency(currency) {
    await fetch(`${API_URL}/currencies/update`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(currency)
    });
    
}

async function deleteCurrency(currency) {
    await fetch(`${API_URL}/currencies/delete`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(currency)
    });
    
}


